package com.citi.training.product.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.product.dao.ProductDao;
import com.citi.training.product.model.Product;
import com.citi.training.product.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Product> findAll() {
        return productService.findAll();
        
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public Product findById(@PathVariable int id) {
        return productService.findById(id);
    }

    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Product> create(@RequestBody Product product) {
        product.setId(productService.create(product));
        return new ResponseEntity<Product>(product, HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
    	productService.deleteById(id);
    }
}
