package com.citi.training.product.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.product.model.Manufacturer;
import com.citi.training.product.service.ManufacturerService;

@RestController
@RequestMapping("/manufacturer")
public class ManufacturerController {
	@Autowired
	ManufacturerService manufacturerService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Manufacturer> findAll() {
		return  manufacturerService.findAll();

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Manufacturer findById(@PathVariable int id) {
		return  manufacturerService.findById(id);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Manufacturer> create(@RequestBody Manufacturer manufacturer) {
		manufacturer.setId( manufacturerService.create(manufacturer));
		return new ResponseEntity<Manufacturer>(manufacturer, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable int id) {
		 manufacturerService.deleteById(id);
	}

}
