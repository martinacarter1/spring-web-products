package com.citi.training.product.dao;

import java.util.List;

import com.citi.training.product.model.Product;

public interface ProductDao {

    List<Product> findAll();
    int create(Product product);
    Product findById(int id);
    void deleteById(int id);
}
