package com.citi.training.product.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.citi.training.product.model.Manufacturer;

public class MysqlManufacturerDao {
	@Autowired
	JdbcTemplate tpl;
	
	public List<Manufacturer> findAll(){
		return tpl.query("select id, name, price from product", new ManufacturerMapper());}

	public Manufacturer findById(int id) {
        List<Manufacturer> manufacturers = this.tpl.query(
                "select id, name, price from product where id = ?",
                new Object[]{id},
                new ManufacturerMapper()
        );
        if(manufacturers.size() <= 0) {
            throw null;//new EmployeeNotFoundException("Employee with id=" + id + " not found");
        }
        return manufacturers.get(0);
	}

	public int create(Manufacturer manufacturer) {
		return 0;}

	public void deleteById(int id) {}
	
	private static final class ManufacturerMapper implements RowMapper<Manufacturer> {
		public Manufacturer mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Manufacturer(rs.getInt("id"), rs.getString("name"), rs.getString("address"));
		}
	}
}
