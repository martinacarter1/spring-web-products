package com.citi.training.product.dao;

import java.util.List;

import com.citi.training.product.model.Manufacturer;

public interface ManufacturerDao {
	   List<Manufacturer> findAll();
	    int create(Manufacturer manufacturer);
	    Manufacturer findById(int id);
	    void deleteById(int id);
}
