package com.citi.training.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.citi.training.product.dao.ManufacturerDao;
import com.citi.training.product.model.Manufacturer;


public class ManufacturerService {
	@Autowired
	ManufacturerDao manufacturerDao;

	public List<Manufacturer> findAll() {
		return manufacturerDao.findAll();
	}

	public int create(Manufacturer manufacturer) {return manufacturerDao.create(manufacturer);}

	public Manufacturer findById(int id) {return manufacturerDao.findById(id);}

	public void deleteById(int id) {manufacturerDao.deleteById(id);}
}
