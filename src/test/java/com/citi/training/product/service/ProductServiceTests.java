package com.citi.training.product.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.product.dao.ProductDao;
import com.citi.training.product.model.Product;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {
	@Autowired
	private ProductService productService;
	
	@MockBean
	private ProductDao mockProductDao;
	
	@Test
	public void test_createRuns() {
		int newID = 1;
		Product otherProduct = new Product(3,"Dip",1.00);
		when(mockProductDao.create(any(Product.class))).thenReturn(newID);
		int createdID = productService.create(new Product(newID,"Chips", 3.00));
		verify(mockProductDao).create(otherProduct);
		assertEquals(newID,createdID);
	}
	@Test
	public void test_deleteRuns() {
		int id= 2;
		//when(mockProductDao.deleteById(id)).then
		productService.deleteById(id);
		verify(mockProductDao).deleteById(id);
	}
	
	@Test
	public void test_findByID() {
		int testID = 3;
		Product testProduct = new Product(testID, "Beans", 0.99);
		Product returnedProduct = productService.findById(testID);
		
		assertEquals(testProduct,returnedProduct);
		
	}
	@Test
	public void test_findAll() {
		Product otherProduct = new Product(3,"Dip",1.00);
		List<Product> products;
		
	}

}
